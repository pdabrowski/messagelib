/*
  SPDX-FileCopyrightText: 2014-2020 Laurent Montel <montel@kde.org>

  SPDX-License-Identifier: LGPL-2.0-or-later

*/

#ifndef QUICKSEARCHLINE_H
#define QUICKSEARCHLINE_H

#include <QWidget>
#include "messagelist_export.h"
#include <Akonadi/KMime/MessageStatus>

class QComboBox;
class QToolButton;
namespace MessageList {
namespace Core {
class SearchLineStatus;
/**
 * @brief The QuickSearchLine class
 * @author Laurent Montel <montel@kde.org>
 */
class MESSAGELIST_EXPORT QuickSearchLine : public QWidget
{
    Q_OBJECT
public:
    explicit QuickSearchLine(QWidget *parent = nullptr);
    ~QuickSearchLine() override;

    enum SearchOption {
        SearchEveryWhere = 1,
        SearchAgainstBody = 2,
        SearchAgainstSubject = 4,
        SearchAgainstFrom = 8,
        SearchAgainstBcc = 16,
        SearchAgainstTo = 32
    };

    Q_ENUM(SearchOption)
    Q_DECLARE_FLAGS(SearchOptions, SearchOption)

    Q_REQUIRED_RESULT SearchOptions searchOptions() const;

    void focusQuickSearch(const QString &selectedText);

    Q_REQUIRED_RESULT QComboBox *tagFilterComboBox() const;
    Q_REQUIRED_RESULT SearchLineStatus *searchEdit() const;
    void resetFilter();
    Q_REQUIRED_RESULT QVector<Akonadi::MessageStatus> status() const;

    void updateComboboxVisibility();

    Q_REQUIRED_RESULT bool containsOutboundMessages() const;
    void setContainsOutboundMessages(bool containsOutboundMessages);

    void changeQuicksearchVisibility(bool show);
    void addCompletionItem(const QString &str);

Q_SIGNALS:
    void clearButtonClicked();
    void searchEditTextEdited(const QString &);
    void searchOptionChanged();
    void statusButtonsClicked();
    void forceLostFocus();

protected:
    bool eventFilter(QObject *object, QEvent *e) override;
private Q_SLOTS:
    void slotSearchEditTextEdited(const QString &text);
    void slotClearButtonClicked();
    void slotFilterActionChanged(const QVector<Akonadi::MessageStatus> &lst);
private:
    SearchLineStatus *mSearchEdit = nullptr;
    QComboBox *mTagFilterCombo = nullptr;
    QVector<Akonadi::MessageStatus> mLstStatus;
};
}
}
#endif // QUICKSEARCHLINE_H
