/*
   SPDX-FileCopyrightText: 2015-2020 Laurent Montel <montel@kde.org>

   SPDX-License-Identifier: LGPL-2.0-or-later
*/

#ifndef HEADERSTYLEINTERFACE_H
#define HEADERSTYLEINTERFACE_H

#include <QObject>
#include "messageviewer_export.h"
class KToggleAction;
class KActionCollection;
class QAction;
class QActionGroup;
class KActionMenu;
namespace MessageViewer {
class HeaderStyle;
class HeaderStylePlugin;
/**
 * @brief The HeaderStyleInterface class
 * @author Laurent Montel <montel@kde.org>
 */
class MESSAGEVIEWER_EXPORT HeaderStyleInterface : public QObject
{
    Q_OBJECT
public:
    explicit HeaderStyleInterface(MessageViewer::HeaderStylePlugin *, QObject *parent = nullptr);
    ~HeaderStyleInterface() override;
    Q_REQUIRED_RESULT QList<KToggleAction * > action() const;
    virtual void createAction(KActionMenu *menu, QActionGroup *actionGroup, KActionCollection *ac) = 0;
    virtual void activateAction() = 0;
    Q_REQUIRED_RESULT HeaderStylePlugin *headerStylePlugin() const;

Q_SIGNALS:
    void styleChanged(MessageViewer::HeaderStylePlugin *plugin);
    void styleUpdated();

protected Q_SLOTS:
    void slotStyleChanged();
protected:
    void addHelpTextAction(QAction *act, const QString &text);
    void addActionToMenu(KActionMenu *menu, QActionGroup *actionGroup);
    QList<KToggleAction *> mAction;
    HeaderStylePlugin *const mHeaderStylePlugin;
};
}
#endif // HEADERSTYLEINTERFACE_H
