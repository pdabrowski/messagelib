/*
   SPDX-FileCopyrightText: 2017-2020 Laurent Montel <montel@kde.org>

   SPDX-License-Identifier: LGPL-2.0-or-later
*/

#ifndef TEMPLATEPARSEREXTRACTHTMLINFO_H
#define TEMPLATEPARSEREXTRACTHTMLINFO_H

#include <QObject>
#include "templateparser_private_export.h"
#include "templateparserextracthtmlinforesult.h"

namespace TemplateParser {
class TemplateExtractTextFromMail;
class TemplateExtractHtmlElementFromMail;

class TEMPLATEPARSER_TESTS_EXPORT TemplateParserExtractHtmlInfo : public QObject
{
    Q_OBJECT
public:
    explicit TemplateParserExtractHtmlInfo(QObject *parent = nullptr);
    ~TemplateParserExtractHtmlInfo() override;

    void setHtmlForExtractingTextPlain(const QString &html);
    void setHtmlForExtractionHeaderAndBody(const QString &html);
    void setTemplate(const QString &str);
    void start();

Q_SIGNALS:
    void finished(const TemplateParserExtractHtmlInfoResult &result);

private:
    void slotExtractHtmlElementFinished(bool success);
    void slotExtractToPlainTextFinished(bool success);

    TemplateParserExtractHtmlInfoResult mResult;

    QString mHtmlForExtractingTextPlain;
    QString mHtmlForExtractionHeaderAndBody;

    QString mTemplateStr;

    TemplateExtractTextFromMail *mTemplateWebEngineView = nullptr;
    TemplateExtractHtmlElementFromMail *mExtractHtmlElementWebEngineView = nullptr;
};
}
#endif // TEMPLATEPARSEREXTRACTHTMLINFO_H
